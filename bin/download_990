#!/usr/bin/env python3

# Copyright (C) 2019  Martin Michlmayr <tbm@cyrius.com>
# License: GNU General Public License (GPL), version 3 or above
# SPDX-License-Identifier: GPL-3.0-or-later

"""
Load information about organizations from orgs.yaml, look up 990
forms on 990 Finder, download them (if needed) and show git commit
commands that can be run after reviewing the files.
"""

from pathlib import Path
import yaml

import requests
from bs4 import BeautifulSoup


def process_year(short, year, download, form):
    """Downloads 990 PDF for one year, if not already on disk"""
    if not form.startswith("990"):
        print(f"Unknown form {form}")
        return
    request_990 = requests.get(download, timeout=10)
    filepath = Path("Form990s") / Path(
        f"{short}_{year}_form-990.pdf".lower().replace(" ", "-")
    )
    if filepath.exists():
        if int(request_990.headers["Content-Length"]) != filepath.stat().st_size:
            print(f"File {filepath} does not match")
    else:
        with open(filepath, "wb") as pdf_file:
            pdf_file.write(request_990.content)
        print(f"git add {filepath}")
        print(f'git commit -S -m "Add {form} from {short} for {year}" {filepath}')


def process_org(name, ean, short=None):
    """Looks for 990 PDFs for an org and initiates download"""
    print(name)
    url_990finder = "https://990finder.foundationcenter.org/990results.aspx"
    request_finder = requests.get(
        url_990finder, params={"ei": ean.replace("-", "")}, timeout=10
    )
    soup = BeautifulSoup(request_finder.text, "html.parser")
    table = soup.find("table", {"id": "MainContent_GridView1"})
    todo = []
    for row in table.findAll("tr", {"bgcolor": {"#ECEEF2", "White"}}):
        cols = row.find_all("td")
        download = "https:" + cols[0].a["href"]
        year = int(cols[2].text.strip())
        form = cols[3].text.strip()
        todo.append((year, download, form))
    for info in sorted(todo):
        process_year(short or name, *info)


def process_all():
    """Load orgs from config file and process then"""
    with open("orgs.yaml", encoding="utf-8") as config_file:
        orgs = yaml.load(config_file, Loader=yaml.FullLoader)
    for org in orgs:
        process_org(org, **orgs[org])


if __name__ == "__main__":
    process_all()
